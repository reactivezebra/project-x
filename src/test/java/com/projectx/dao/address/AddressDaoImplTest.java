package com.projectx.dao.address;

import com.projectx.config.JPAConfig;
import com.projectx.config.MvcConfig;
import com.projectx.config.ServiceConfig;
import com.projectx.intitializer.WebAppInializer;
import com.projectx.model.Address;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by podo on 14.05.15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppInializer.class, JPAConfig.class,MvcConfig.class, ServiceConfig.class})
@WebAppConfiguration
@ActiveProfiles("podo")
public class AddressDaoImplTest
{
    @Autowired
    private AddressDAO addressDao;

    @Test
    public void testFindByCountry()
    {
        Address address = addressDao.findByCountry("Ukraine");
        Assert.assertNotNull(address);
        Assert.assertEquals("Ukraine", address.getCountry());
    }
}
