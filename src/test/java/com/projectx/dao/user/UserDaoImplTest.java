package com.projectx.dao.user;

import com.projectx.config.JPAConfig;
import com.projectx.config.MvcConfig;
import com.projectx.config.ServiceConfig;
import com.projectx.intitializer.WebAppInializer;
import com.projectx.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by podo on 14.05.15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppInializer.class, JPAConfig.class, MvcConfig.class, ServiceConfig.class})
@WebAppConfiguration
@ActiveProfiles("podo")
public class UserDaoImplTest
{
    @Autowired
    UserDao userDao;

    @Test
    public void testFindByUsername()
    {
        User user = userDao.findByUsername("Andreu");
        User user1 = userDao.findByUsername("And");

        Assert.assertEquals("Andreu", user.getUsername());
        Assert.assertNull(user1);
        System.out.println(user1);
    }

    @Test
    public void testFindAll(){
        List<User> users = userDao.findAll();
        Assert.assertTrue(!users.isEmpty());
    }

    @Test
    public void testFindById()
    {
        User user = userDao.find(1l);
        User user1 = userDao.find(3L);
        Assert.assertNotNull(user);
        Assert.assertEquals("Andreu", user.getUsername());
        Assert.assertNull(user1);
    }

    @Test
    public void testSave()
    {
//        User user = new User("an@S.ds", "775166s");
//        user.setUsername("Igor");
//
//        User user1 = userDao.save(user);
//
//        User testUser = userDao.findByUsername("Igor");
//        Assert.assertEquals(user1.getUsername(), testUser.getUsername());
        //РАБОТАЕТ
    }

    @Test
    public void testRemove()
    {

//        userDao.delete(5L);
//        User user = userDao.find(5L);
//        Assert.assertNull(user);

        //РАБОАЕТ
    }

    @Test
    public void testFindByEmail()
    {
        User user = userDao.findByEmail("asd");
        Assert.assertNotNull(user);
        Assert.assertEquals("asd", user.getEmail());
        Assert.assertNotEquals("asds", user.getEmail());
    }
}
