package com.projectx.config.datasource;

import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 * Created with IntelliJ IDEA.
 * User: Alk
 * Date: 15.05.15
 * Time: 9:47
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public interface DataConfig
{
    LocalContainerEntityManagerFactoryBean entityManagerFactoryBean();
}
