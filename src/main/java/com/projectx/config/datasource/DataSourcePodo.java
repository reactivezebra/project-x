package com.projectx.config.datasource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Alk
 * Date: 15.05.15
 * Time: 9:48
 * To change this template use File | Settings | File Templates.
 */
@Configuration
@Profile("podo")
@PropertySource(
        {"classpath:META-INF/db/data-access.properties",
                "classpath:META-INF/hibernate.properties"})
public class DataSourcePodo implements DataConfig
{
    @Autowired
    private org.springframework.core.env.Environment env;

    @Bean(name = "dataSource")
    public DataSource configureDataSource()
    {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        config.setJdbcUrl(env.getProperty("jdbc.databaseUrl"));
        config.setUsername(env.getProperty("jdbc.username"));
        config.setPassword(env.getProperty("jdbc.password"));
        config.setConnectionTestQuery("show tables");
        return new HikariDataSource(config);
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean()
    {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(configureDataSource());
        entityManagerFactory.setPackagesToScan("com.projectx.model");
        entityManagerFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        Properties jpaProperties = new Properties();
        jpaProperties.put(Environment.DIALECT, env.getProperty("hibernate.dialect"));
        jpaProperties.put(Environment.HBM2DDL_AUTO, env.getProperty("hibernate.hbm2ddl.auto"));
        jpaProperties.put(Environment.SHOW_SQL, true);

        entityManagerFactory.setJpaProperties(jpaProperties);
        return entityManagerFactory;
    }
}
