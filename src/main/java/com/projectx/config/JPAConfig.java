package com.projectx.config;

import com.projectx.config.datasource.DataConfig;
import com.projectx.config.utils.JinqSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.projectx.model", "com.projectx.dao","com.projectx.config.datasource"})
public class JPAConfig
{
    @Autowired
    DataConfig dataConfig;

    @Bean
    public PlatformTransactionManager annotationDrivenTransactionManager()
    {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(dataConfig.entityManagerFactoryBean().getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation()
    {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean(name = "jinqSource")
    public JinqSource getJinqSource()
    {
        return new JinqSource();
    }
}
