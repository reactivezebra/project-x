package com.projectx.config.utils;

import org.jinq.jpa.JinqJPAStreamProvider;
import org.jinq.orm.stream.JinqStream;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 * Created by podo on 13.05.15.
 */
@Component
public class JinqSource
{
    private JinqJPAStreamProvider streams;

    @PersistenceUnit
    private void setEntityManagerFactory(
            EntityManagerFactory emf) throws Exception
    {
        streams = new JinqJPAStreamProvider(emf);
        // Do any additional Jinq initialization needed here.
    }

    public <U> JinqStream<U> streamAll(EntityManager em, Class<U>entity)
    {
        return streams.streamAll(em, entity);
    }
}

