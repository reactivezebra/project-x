package com.projectx.service.user;

import com.projectx.dao.user.UserDao;
import com.projectx.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by podo on 14.05.15.
 */
@Service("userService")
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserDao userDao;

    @Override
    public User findByUsername(String username)
    {
        return userDao.findByUsername(username);
    }

    @Override
    public User findByEmail(String email)
    {
        return userDao.findByEmail(email);
    }

    @Override
    public List<User> findAll()
    {
        return userDao.findAll();
    }

    @Override
    public User find(Long id)
    {
        return userDao.find(id);
    }

    @Override
    public User save(User item)
    {
        return userDao.save(item);
    }

    @Override
    public void delete(Long id)
    {
        userDao.delete(id);
    }
}
