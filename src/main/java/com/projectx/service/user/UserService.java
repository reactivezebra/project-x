package com.projectx.service.user;

import com.projectx.model.User;
import com.projectx.service.Service;

import java.util.List;

/**
 * Created by podo on 14.05.15.
 */
public interface UserService extends Service<User, Long>
{
    User findByUsername(String username);

    User findByEmail(String email);
}
