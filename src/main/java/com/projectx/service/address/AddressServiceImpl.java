package com.projectx.service.address;

import com.projectx.dao.address.AddressDAO;
import com.projectx.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by podo on 05.05.15.
 */
@Service("addressService")
public class AddressServiceImpl implements AddressService
{
    @Autowired
    private AddressDAO addressDao;

    @Override
    public List<Address> findAll()
    {
        return addressDao.findAll();
    }

    @Override
    public Address find(Long id)
    {
        return addressDao.find(id);
    }

    @Override
    public Address save(Address item)
    {
        return addressDao.save(item);
    }

    @Override
    public void delete(Long id)
    {
        addressDao.delete(id);
    }

    @Override
    public Address findByCountry(String country)
    {
        return addressDao.findByCountry(country);
    }
}
