package com.projectx.service.address;

import com.projectx.model.Address;
import com.projectx.service.Service;

/**
 * Created by podo on 05.05.15.
 */
public interface AddressService extends Service<Address, Long>
{
    Address findByCountry(String country);
}
