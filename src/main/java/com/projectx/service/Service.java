package com.projectx.service;

import com.projectx.model.Entity;

import java.util.List;

/**
 * Created by podo on 14.05.15.
 */
public interface Service<E extends Entity, I>
{
    List<E> findAll();

    E find(I id);

    E save(E item);

    void delete(I id);
}
