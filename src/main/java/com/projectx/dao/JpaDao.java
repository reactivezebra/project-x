package com.projectx.dao;

import com.projectx.config.utils.JinqSource;
import com.projectx.model.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by podo on 14.05.15.
 */
public class JpaDao<E extends Entity, I> implements Dao<E, I>
{
    private JinqSource jinqSource;
    private EntityManager entityManager;
    private Class<E> entityClass;

    public JpaDao(Class<E> entityClass)
    {
        this.entityClass = entityClass;
    }

    protected JinqSource getJinqSource()
    {
        return this.jinqSource;
    }

    protected EntityManager getEntityManager()
    {
        return this.entityManager;
    }

    @Autowired
    private void setJinqSource(JinqSource jinqSource)
    {
        this.jinqSource = jinqSource;
    }

    @PersistenceContext
    private void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> findAll()
    {
        return getJinqSource().streamAll(getEntityManager(), entityClass).toList();
    }

    @Override
    @Transactional(readOnly = true)
    public E find(I id)
    {
        return getEntityManager().find(entityClass, id);
    }

    @Override
    @Transactional
    public E save(E item)
    {
        return this.getEntityManager().merge(item);
    }

    @Override
    @Transactional
    public void delete(I id) {
        if(id == null){
            return;
        }

        E entity = this.find(id);
        if(entity == null){
            return;
        }

        this.getEntityManager().remove(entity);
    }
}
