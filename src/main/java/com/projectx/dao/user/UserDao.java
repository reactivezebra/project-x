package com.projectx.dao.user;

import com.projectx.dao.Dao;
import com.projectx.model.User;

/**
 * Created by podo on 13.05.15.
 */
public interface UserDao extends Dao<User, Long>
{
    User findByUsername(String username);

    User findByEmail(String email);
}
