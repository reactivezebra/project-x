package com.projectx.dao.user;


import com.projectx.dao.JpaDao;
import com.projectx.model.User;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by podo on 14.05.15.
 */
@Repository("userDao")
public class UserDaoImpl extends JpaDao<User, Long> implements UserDao
{
    public UserDaoImpl()
    {
        super(User.class);
    }

    @Override
    public User findByUsername(String username)
    {
        List<User> users = getJinqSource().streamAll(getEntityManager(), User.class)
                                          .where(u -> u.getUsername().equals(username))
                                          .toList();
        if(users.isEmpty()) return null;
        return users.get(0);

    }

    @Override
    public User findByEmail(String email)
    {
        List<User> users = getJinqSource().streamAll(getEntityManager(), User.class)
                                          .where(u -> u.getEmail().equals(email))
                                          .toList();
        if(users.isEmpty()) return null;
        return users.get(0);
     }
}
