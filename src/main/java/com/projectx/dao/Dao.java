package com.projectx.dao;


import com.projectx.model.Entity;
import java.util.List;

/**
 * Created by podo on 13.05.15.
 */
public interface Dao<E extends Entity, I>
{
    List<E> findAll();

    E find(I id);

    E save(E item);

    void delete(I id);
}
