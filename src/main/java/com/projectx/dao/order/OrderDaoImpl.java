package com.projectx.dao.order;

import com.projectx.dao.JpaDao;
import com.projectx.model.Order;
import org.springframework.stereotype.Repository;

/**
 * Created by podo on 14.05.15.
 */
@Repository("orderDao")
public class OrderDaoImpl extends JpaDao<Order, Long> implements OrderDao
{

    public OrderDaoImpl()
    {
        super(Order.class);
    }

    @Override
    public Order findByCust(String customer)
    {
        return getJinqSource().streamAll(getEntityManager(), Order.class).where(c -> c.getCustomer().equals("Andreu")).toList().get(0);
    }
}
