package com.projectx.dao.order;

import com.projectx.model.Order;

/**
 * Created by podo on 14.05.15.
 */
public interface OrderDao
{
    Order findByCust(String customer);
}
