package com.projectx.dao.address;

import com.projectx.dao.Dao;
import com.projectx.model.Address;

/**
 * Created by podo on 14.05.15.
 */
public interface AddressDAO extends Dao<Address, Long>
{
    Address findByCountry(String country);
}
