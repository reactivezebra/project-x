package com.projectx.dao.address;

import com.projectx.dao.JpaDao;
import com.projectx.model.Address;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by podo on 14.05.15.
 */
@Repository("addressDao")
public class AddressDAOImpl extends JpaDao<Address, Long> implements AddressDAO
{
    public AddressDAOImpl()
    {
        super(Address.class);
    }

    @Override
    public Address findByCountry(String country)
    {
        List<Address> addresses = getJinqSource().streamAll(getEntityManager(), Address.class)
                                                 .where(a -> a.getCountry().equals(country))
                                                 .toList();
        if(addresses.isEmpty()) return null;
        return addresses.get(0);
    }
}
