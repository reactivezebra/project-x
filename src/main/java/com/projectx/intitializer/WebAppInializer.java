package com.projectx.intitializer;

import com.projectx.config.JPAConfig;
import com.projectx.config.MvcConfig;
import com.projectx.config.ServiceConfig;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * Created by podo on 25.03.15.
 */

public class WebAppInializer implements WebApplicationInitializer
{
    @Override
    public void onStartup(ServletContext conteiner) throws ServletException
    {
        //Spring root application context (тут еще добавить конфиг для бд)
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(ServiceConfig.class, JPAConfig.class);
        rootContext.getEnvironment().setActiveProfiles("podo");
        //Manage the lifecycle of the root application context
        conteiner.addListener(new ContextLoaderListener(rootContext));

        // Create the dispatcher servlet's Spring application context
        AnnotationConfigWebApplicationContext dispatcherServlet = new AnnotationConfigWebApplicationContext();
        dispatcherServlet.register(MvcConfig.class);

        //Register and map the dispatcher servlet
        ServletRegistration.Dynamic dispatcher = conteiner.addServlet("dispatcher", new DispatcherServlet(dispatcherServlet));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
    }
}
