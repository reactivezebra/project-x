package com.projectx.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by podo on 31.03.15.
 */

@Controller
public class IndexController
{
    @RequestMapping("/")
    public String getIndex()
    {
        return "index";
    }

}
