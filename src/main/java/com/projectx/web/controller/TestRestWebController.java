package com.projectx.web.controller;


import com.projectx.model.Address;
import com.projectx.model.TestStory;
import com.projectx.model.User;
import com.projectx.service.address.AddressService;
import com.projectx.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by podo on 25.03.15.
 */
@RestController
public class TestRestWebController
{

    private static final Logger logger = LoggerFactory.getLogger(TestRestWebController.class);

    @Autowired
    private AddressService addressService;

    @Autowired
    private UserService userService;

    @RequestMapping("/st")
    public List<TestStory> getUser()
    {
        List<TestStory> stories = new ArrayList<>();
        TestStory story1 = new TestStory("Story 00", "Description pending.");
        TestStory story2 = new TestStory("Story 01", "Description pending.");
        TestStory story3 = new TestStory("Story 02", "Description pending.");
        TestStory story4 = new TestStory("Story 03", "Description pending.");
        TestStory story5 = new TestStory("Story 04", "Description pending.");
        TestStory story6 = new TestStory("Story 05", "Description pending.");
        TestStory story7 = new TestStory("Story 06", "Description pending.");
        TestStory story8 = new TestStory("Story 07", "Description pending.");

        stories.add(story1);
        stories.add(story2);
        stories.add(story3);
        stories.add(story4);
        stories.add(story5);
        stories.add(story6);
        stories.add(story7);

        return stories;
    }

    @RequestMapping("/ad")
    public List<Address> getAddresses()
    {
        return addressService.findAll();
    }

    @RequestMapping("/user")
    public User getUserByName(){return userService.findByUsername("Andreu");}

}
