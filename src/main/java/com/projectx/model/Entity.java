package com.projectx.model;

import java.io.Serializable;

/**
 * Created by podo on 13.05.15.
 */
public interface Entity extends Serializable
{
     Long getId();
}
