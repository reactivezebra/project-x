package com.projectx.model;

/**
 * Created by podo on 30.03.15.
 */
public class TestStory
{
    private String title;
    private String description;


    public TestStory(String title, String description)
    {
        this.title = title;
        this.description = description;
    }


    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
