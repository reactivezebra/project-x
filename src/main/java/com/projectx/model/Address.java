package com.projectx.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by podo on 21.04.15.
 */
@Entity
@Table(name = "address")
public class Address implements com.projectx.model.Entity
{
    @Id
    private Long id;

    private String country;
    private String city;
    private String street;
    private String appartment;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getAppartment()
    {
        return appartment;
    }

    public void setAppartment(String appartment)
    {
        this.appartment = appartment;
    }
}
