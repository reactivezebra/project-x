package com.projectx.model;


import javax.persistence.*;
import javax.persistence.Entity;

/**
 * Created by podo on 21.04.15.
 */
@Table(name = "orders")
@Entity
public class Order implements com.projectx.model.Entity
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 16, nullable = false)
    private String customer;

    public String getCustomer()
    {
        return customer;
    }

    public void setCustomer(String customer)
    {
        this.customer = customer;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
